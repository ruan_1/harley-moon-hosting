"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const ip_1 = __importDefault(require("ip"));
const app = (0, express_1.default)();
const port = 3000;
app.get('/', (req, res) => {
    res.send('Hello World!');
});
app.use(express_1.default.static('public'));
app.listen(port, () => {
    console.log(`Files will be hosted at http://${ip_1.default.address()}:${port}`);
    return console.log(`Express is listening at http://127.0.0.1:${port}`);
});
//# sourceMappingURL=index.js.map