
import express from 'express';
import ip from 'ip';

const app = express();
const port = 3000;

app.get('/', (req, res) => {
  res.send('Hello World!');
});

app.use(express.static('public'));

app.listen(port, () => {
  console.log(`Files will be hosted at http://${ip.address()}:${port}/<filename>`);
  return console.log(`Express is listening at http://127.0.0.1:${port}`);
});